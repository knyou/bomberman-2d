//un block fait 16*16 pixels agrandit par 2.
class Ensemble_Block {
	constructor(url) {
	this.image = new Image();
	this.image.copie = this;
	this.image.onload = function() {
		this.copie.largeur = this.width / 16;
	}
	this.image.src = url;
	};


//un block est une partie d'une "matrice" de block
//un block a un num en fonction de sa place dans la matrice. 
// les blocks sont numérotés suivant la logique:
// 1 2 3 4 
// 5 6 7 8
// ....
draw (ctx, num, dest_x, dest_y) {

var colonne = num % this.largeur;
if(colonne == 0) colonne = this.largeur; 
colonne = colonne -1;
var ligne_tempo = (num / this.largeur);
var ligne = Math.trunc(ligne_tempo);
if (ligne == ligne_tempo) {--ligne;}
var x = colonne * 16;
var y = ligne * 16;
ctx.drawImage(this.image, x, y, 16, 16, dest_x, dest_y, 32, 32);// on agrandit de 2 fois le block pour l'affichage 
	};
};


function Terrain () {
var requete = new XMLHttpRequest(); 
requete.onload = function()
{

if (requete.readyState == 4 && requete.status == 200) {}

else {
	throw new Error("erreur chargement carte");}

};

requete.open("GET",  'json' + '.json',false);
    
requete.send();

var Json_terrain = requete.responseText;
var Json_parse = JSON.parse(Json_terrain);
this.blocks = new Ensemble_Block(Json_parse.blocks);
this.terrain = Json_parse.terrain;
this.hauteur = this.terrain.length;
this.largeur = this.terrain[0].length;
};



Terrain.prototype.get_contenu_case = function(i,j) {
	if (i<0 || j< 0 || j > this.largeur - 1 || i > this.hauteur - 1) {return -1;}
	else {return (this.terrain[i])[j];}
}

Terrain.prototype.draw = function(ctx) {

for(var i = 0 ; i < this.hauteur ; i++) {
		
		for(var j = 0 ; j < this.largeur ; j++) {			
			this.blocks.draw( ctx, (this.terrain[i])[j], j * 32, i* 32);	
		}
	}



};
	
Terrain.prototype.getH = function() {
	return this.hauteur;
}
Terrain.prototype.getL = function() {
	return this.largeur;
}

function Perso (x,y,terrain,vie,taille_bombe) {
this.taille_bombe = taille_bombe;

this.vie = vie;
this.animation_en_cours = false;		//gestion des acces concurrents a l'animation du perso 

this.terrain = terrain;
this.x = x;
this.y = y;

this.image = new Image();
this.image.src = "goblin.png";

}


Perso.prototype.set_bombe = function(taille) {

	this.taille_bombe = taille;

}

Perso.prototype.get_bombe = function() {

	return this.taille_bombe;

}

//j'utilise un sprite pour les perso
Perso.prototype.draw= function(ligne,colonne,ctx) {
var x = colonne * 64;//quels mouv du perso on
var y = ligne * 64;//veut dessiner: avancer, reculer...
ctx.drawImage(this.image, x, y, 64, 64, (this.x)*32, (this.y)*32, 32, 32);

}

Perso.prototype.move = function(x,y) { 
	if (this.x + x <= this.terrain.getL() -1 && this.x + x >=0 && this.y + y >=0 && this.y + y <= this.terrain.getH() - 1)
		{this.x  = (16*this.x +16*x)/16; this.y = (16*this.y +16*y)/16;}

}


Perso.prototype.get_y = function() {

	return this.y;
}



Perso.prototype.get_x = function() {

	return this.x;
}


Perso.prototype.get_anim_en_cours = function() {

	return this.animation_en_cours;
}


Perso.prototype.set_anim_en_cours = function(boolean) {

	this.animation_en_cours = boolean;
}




Perso.prototype.mov_step = function(x,y) {

	this.move(x*(0.0625), y*(0.0625));

}




function Bombe(x,y,rayon,terrain, player) {
this.player = player;
this.terrain = terrain;
this.x = x;
this.y = y;
this.rayon = rayon;
this.image = new Image();
this.image.src = "bomb_party.png";

}


Bombe.prototype.get_x = function() {

	return this.x;

}


Bombe.prototype.set_x_y = function(x,y) {

	this.x = x;
	this.y = y;

}


Bombe.prototype.get_y = function() {

	return this.y;

}

Bombe.prototype.draw = function(avancement,ctx) {
	
	var ligne = 18;
	var colonne = 4 + avancement;
	var x = colonne * 16;
	var y = ligne *16;
	ctx.drawImage(this.image, x, y, 16, 16, (this.x)*32 + 8 , (this.y)*32 +8, 16, 16);
}

Bombe.prototype.draw_explo = function(ctx) {

var partie_terminee = false;

ctx.drawImage(this.image, 2*16, 18*16, 16, 16, this.x*32, this.y*32, 32, 32);

if(this.x === this.player.get_x() && this.y === this.player.get_y()) {partie_terminee = true;}

var longueur = this.rayon;	

for(var i = this.x +1 ; i< this.x + longueur ; i++) {
	if (i < this.terrain.getL()  && ((this.terrain.terrain)[this.y])[i] === 197) {
		if(i === this.player.get_x() && this.y === this.player.get_y()) {partie_terminee = true;}
		ctx.drawImage(this.image, 16, 18*16, 16, 16, i*32, this.y*32, 32, 32);
	}
	else {  if (i < this.terrain.getL()  && ((this.terrain.terrain)[this.y])[i] === 205) {
			this.terrain.terrain[this.y][i] = 197 ;
		} 

		 break;}

}		

for(var i = this.x -1 ; i> this.x - longueur ; i--) {
	if (i > -1  && ((this.terrain.terrain)[this.y])[i] === 197) {
		if(i === this.player.get_x() && this.y === this.player.get_y()) {partie_terminee = true;}
		ctx.drawImage(this.image, 16, 18*16, 16, 16, i*32, this.y*32, 32, 32);
	}
	else {  if (i > -1  && ((this.terrain.terrain)[this.y])[i] === 205) {
			this.terrain.terrain[this.y][i] = 197 ;}

		break;}

}		

for(var j = this.y +1 ; j< this.y + longueur ; j++) {
	if (j < this.terrain.getH()  && ((this.terrain.terrain)[j])[this.x] === 197) {
		if(this.x === this.player.get_x() && j === this.player.get_y()) {partie_terminee = true;}
		ctx.drawImage(this.image, 14*16, 14*16, 16, 16, this.x*32, j*32, 32, 32);
	}
	else { if (j < this.terrain.getH()  && ((this.terrain.terrain)[j])[this.x] === 205) {
			this.terrain.terrain[j][this.x] = 197 ;}

		break;}

}		

for(var j = this.y -1 ; j> this.y - longueur ; j--) {
	if (j > -1  && ((this.terrain.terrain)[j])[this.x] === 197) {
		if(this.x === this.player.get_x() && j === this.player.get_y()) {partie_terminee = true;}
		ctx.drawImage(this.image, 14*16, 14*16, 16, 16, this.x*32, j*32, 32, 32);
	}
	else { if (j > -1  && ((this.terrain.terrain)[j])[this.x] === 205) {
			this.terrain.terrain[j][this.x] = 197 ;}

		break;}

}	

if (partie_terminee) {ctx.fillText("JOUEUR TOUCHE", (this.player.get_x())*32, (this.player.get_y())*32,100);}

}



var map = new Terrain();
var per = new Perso (2,2,map,1,3);
var bom = new Bombe(3,3,3,map,per);

window.onload = function() {
	var canvas = document.getElementById('canvas');
	var ctx = canvas.getContext('2d');
	canvas.width  = map.getL() * 32;
	canvas.height = map.getH() * 32;
	map.draw(ctx);
	per.draw(1,1,ctx);

var l = 0;
var c = 0;
var x = 0;
var y = 0;


var progress = 0;
var progress_bombe = 0;
var progress_explo = 0;
var bomb_timer = 0;
var explo_timer = 0;
//fonction qui dessine la transition du perso d'une case à une autre. Une transition fait 10 images (possible d'adapter en fonction
//de la vitesse que l'on veut donner au perso)
function animation(id) {
	if(progress<16){	
	++progress;
	map.draw(ctx);	
	per.mov_step(x,y);
	if (bomb_timer === 1) {bom.draw(0,ctx);}
	if (explo_timer === 1) {bom.draw_explo(ctx);}
	per.draw(l,c,ctx);
	requestAnimationFrame(animation);}
	else {progress = 0; per.set_anim_en_cours(false);
	      console.log(per.get_x()); console.log(per.get_y());	}

}

function animation_explo() {

		explo_timer = 1;
	if( progress_explo < 25) {
		++progress_explo;
		bom.draw_explo(ctx);
		requestAnimationFrame(animation_explo);
		
	}
	else {	explo_timer = 0;  
		map.draw(ctx);
		per.draw(l,c,ctx);		
		progress_explo = 0;}	

}

function animation_bombe() {

	if( progress_bombe < 100) {
		//if (progress_bombe%5 ==0 && progress_bombe!= 0)  {++bomb_timer}		
		++progress_bombe;
		bom.draw(0,ctx);
		requestAnimationFrame(animation_bombe);
		
	}
	else {	 
		bomb_timer = 0;
		map.draw(ctx);
		per.draw(l,c,ctx);	
		progress_bombe = 0
		requestAnimationFrame(animation_explo); }	

}




document.onkeydown = function(event) {
	
	console.log(event.keyCode);
	
	switch(event.keyCode) {
	
	case 38 :
		if (per.get_anim_en_cours() === false && 197 === map.get_contenu_case((per.get_y() - 1),(per.get_x()))){	
		per.set_anim_en_cours(true);	
		if (l!=2) {c=0;}
		else {if (c<3){++c;} else {c=0;}}
		l = 2;
		x=0; y = -1; 
		requestAnimationFrame(animation);
		} 
		break;

	case 40 :if (per.get_anim_en_cours() === false && 197 === map.get_contenu_case((per.get_y()+1),(per.get_x()))){	
		per.set_anim_en_cours(true);
		if (l!=0) {c=0;}
		else {if (c<3){++c;} else {c=0;}}
		l=0;
		x=0; y = 1; 
		requestAnimationFrame(animation); }
		break;

	case 39 :if (per.get_anim_en_cours() === false && 197 === map.get_contenu_case((per.get_y()),(per.get_x()+1))){
		per.set_anim_en_cours(true);
		if (l!=1) {c=0;}
		else {if (c<3){++c;} else {c=0;}}
		l=1;
		x=1; y = 0;
		requestAnimationFrame(animation);} 
		break;

	case 37 :if (per.get_anim_en_cours() === false && 197 === map.get_contenu_case((per.get_y()),(per.get_x()-1))){
		per.set_anim_en_cours(true);
		if (l!=3) {c=0;}
		else {if (c<3){++c;} else {c=0;}}		
		l=3;
		x=-1; y = 0; 
		requestAnimationFrame(animation); }
		break;
	case 66 : if (per.get_anim_en_cours() === false) {
			if(bomb_timer === 0) {		  	
			bomb_timer = 1;	
			bom.set_x_y(per.get_x(),per.get_y());
			requestAnimationFrame(animation_bombe); }

		}
	} }
        
};
