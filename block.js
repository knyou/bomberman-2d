//un block fait 16*16 pixels agrandit par 2.
class Ensemble_Block {
	constructor(url) {
	
	this.image = new Image();
	this.image.copie = this;
	this.image.onload = function() {
		this.copie.largeur = this.width / 16;
	}
	this.image.src = url;
	};


//un block est une partie d'une "matrice" de block
//un block a un num en fonction de sa place dans la matrice. 
// les blocks sont numérotés suivant la logique:
// 1 2 3 4 
// 5 6 7 8
// ....
draw (ctx, num, dest_x, dest_y) {

var colonne = num % this.largeur;
if(colonne == 0) colonne = this.largeur; 
colonne = colonne -1;
var ligne_tempo = (num / this.largeur);
var ligne = Math.trunc(ligne_tempo);
if (ligne == ligne_tempo) {--ligne;}
var x = colonne * 16;
var y = ligne * 16;
ctx.drawImage(this.image, x, y, 16, 16, dest_x, dest_y, 32, 32);// on agrandit de 2 fois le block pour l'affichage 
	};
};


function Terrain () {

var requete = new XMLHttpRequest(); 
requete.onload = function()
{

if (requete.readyState == 4 && requete.status == 200) {}

else {
	throw new Error("erreur chargement carte");}

};

requete.open("GET",  'json' + '.json',false);
    
requete.send();

var Json_terrain = requete.responseText;
var Json_parse = JSON.parse(Json_terrain);
this.blocks = new Ensemble_Block(Json_parse.blocks);
this.terrain = Json_parse.terrain;
this.hauteur = this.terrain.length;
this.largeur = this.terrain[0].length;
};

Terrain.prototype.draw = function(ctx) {

for(var i = 0 ; i < this.hauteur ; i++) {
		
		for(var j = 0 ; j < this.largeur ; j++) {			
			this.blocks.draw( ctx, (this.terrain[i])[j], j * 32, i* 32);	
		}
	}



};
	
Terrain.prototype.getH = function() {
	return this.hauteur;
}
Terrain.prototype.getL = function() {
	return this.largeur;
}

function Perso (x,y,terrain) {
this.terrain = terrain;
this.x = x;
this.y = y;

this.image = new Image();
this.image.copie = this;
this.image.src = "goblin.png";

}


Perso.prototype.draw= function(ligne,colonne,ctx) {
var x = colonne * 64;
var y = ligne * 64;
ctx.drawImage(this.image, x, y, 64, 64, (this.x)*32, (this.y)*32, 32, 32);

}

Perso.prototype.move = function(x,y) { 
	if (this.x + x <= this.terrain.getL() -1 && this.x + x >=0 && this.y + y >=0 && this.y + y <= this.terrain.getH() - 1)
		{this.x  = this.x +x; this.y = this.y +y;}

}


function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}


Perso.prototype.deplacement_animation = function(x,y) {
	var i =0;
	
	while(i<10){
	this.move(x*(1/10),y*(1/10));sleep(50);	
	
	i = i+1;
	}



}

var map = new Terrain();
var per = new Perso (2,1,map);
window.onload = function() {
	var canvas = document.getElementById('canvas');
	var ctx = canvas.getContext('2d');
	canvas.width  = map.getL() * 32;
	canvas.height = map.getH() * 32;
	map.draw(ctx);

var l = 0;
var c = 0;

function load() {document.onkeydown = function(event) {
	console.log(event.keyCode);
	switch(event.keyCode) {
	case 38 : 
		if (l!=2) {c=0;}
		else {if (c<3){++c;} else {c=0;}}
		l=2;
		per.deplacement_animation(0,-1);
		break;
	case 40 :
		if (l!=0) {c=0;}
		else {if (c<3){++c;} else {c=0;}}
		l=0;
		per.move(0,1);
		break;
	case 39 :
		if (l!=1) {c=0;}
		else {if (c<3){++c;} else {c=0;}}
		l=1;
		per.move(1,0);
		break;
	case 37 :
		if (l!=3) {c=0;}
		else {if (c<3){++c;} else {c=0;}}		
		l=3;
		per.move(-1,0);
		break;
	} }
  
	map.draw(ctx);
	per.draw(l,c,ctx);
        requestAnimationFrame(load); };
 
 requestAnimationFrame(load);
};
